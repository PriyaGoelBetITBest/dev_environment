Exec { path => ['/usr/bin', '/bin', '/usr/sbin', '/sbin', '/usr/local/bin', '/usr/local/sbin', '/opt/local/bin'] }


if $domain == '' { $domain = 'localhost' }
if $host == '' { $host = 'localhost' }
if $sql_admin_password == '' { $sql_admin_password = 'Regenbogen.' }

if $db_sportsnews_name == '' { $db_name = 'sportnews-stage' }
if $db_sportsnews_dump_location == '' { $db_location  = '/vagrant/db_dumps/sportsnews.sql' }
if $db_sportsnews_username == '' { $username = 'bib-dev-rw' }
#the password hash stands for the password "HNnW7Xss4LpzQ3k8"
if $db_sportsnews_password_hash == '' { $password = '*E7ADDB8B45AF3B778D390D8750F3D5108A604685' }

if $db_bets_name == '' { $db_name = 'bets-stage' }
if $db_bets_dump_location == '' { $db_location  = '/vagrant/db_dumps/bets.sql' }
if $db_bets_username == '' { $username = 'bib-dev-rw' }
#the password hash stands for the password "HNnW7Xss4LpzQ3k8"
if $db_bets_password_hash == '' { $password = '*E7ADDB8B45AF3B778D390D8750F3D5108A604685' }

class nginx {
	package { "nginx":
		ensure => present,
	}
	service { "nginx":
		ensure => "running",
		require => Package["nginx"],
	}
}

class dbsetup {

	class { "::mysql::server":
		root_password => $sql_admin_password,
	    remove_default_accounts => true,
	    override_options => { 'mysqld' => { 'max_connections' => '1024' }, },
	    databases => {
			"${db_sportsnews_name}" => {
				ensure  => 'present',
				charset => 'utf8',
			},
			"${db_bets_name}" => {
				ensure  => 'present',
				charset => 'utf8',
			},
		},
		users => {
			"${db_sportsnews_username}@localhost" => {
				ensure                   => 'present',
				max_connections_per_hour => '0',
				max_queries_per_hour     => '0',
				max_updates_per_hour     => '0',
				max_user_connections     => '0',
				password_hash            => "${db_sportsnews_password_hash}",
			},
			"${db_bets_username}@localhost" => {
				ensure                   => 'present',
				max_connections_per_hour => '0',
				max_queries_per_hour     => '0',
				max_updates_per_hour     => '0',
				max_user_connections     => '0',
				password_hash            => "${db_bets_password_hash}",
			},
		},
		grants => {
			"${db_sportsnews_username}@localhost/${db_sportsnews_name}.*" => {
				ensure     => 'present',
				options    => ['GRANT'],
				privileges => ['SELECT', 'INSERT', 'UPDATE', 'DELETE'],
				table      => "${db_sportsnews_name}.*",
				user       => "${db_sportsnews_username}@localhost",
			},
			"${db_bets_username}@localhost/${db_bets_name}.*" => {
				ensure     => 'present',
				options    => ['GRANT'],
				privileges => ['SELECT', 'INSERT', 'UPDATE', 'DELETE'],
				table      => "${db_bets_name}.*",
				user       => "${db_bets_username}@localhost",
			},
		},
	}
	
	class { "mysql::bindings":
		php_enable => true,
		java_enable => true,
	}

}

class php {
	Package { ensure => installed }
	package { "php5" :}
	package { "php5-cli" :}
	package { "php5-common" :}
	package { "php5-curl" :}
	package { "php5-fpm" :}
	package { "php5-gd" :}
	package { "php5-json" :}
	package { "php5-mcrypt" :}
	package { "php5-memcache" :}
	package { "php5-readline" :}
	package { "phpmyadmin" :}

	service { "php5-fpm":
		ensure => running,
	}
}

class files {
	file { '/etc/nginx/nginx.conf':
		replace => true,
		source => "/vagrant/assets/nginx.conf",
		require => Package["nginx"],
	}
	file { '/etc/php5/fpm/pool.d/www.conf':
		replace => true,
		source => "/vagrant/assets/www.conf",
		require => Package["php5-fpm"],
	}

	exec { "sudo service nginx restart && sudo service php5-fpm restart":
		require => File['/etc/nginx/nginx.conf','/etc/php5/fpm/pool.d/www.conf'],
	}
}

class dbpopulation {
#	package { "inotify-tools":
#		ensure => present,
#	}
	exec {'if sportsnews.sql exist':
		command         => "mysql -uroot -p${sql_admin_password} -h localhost ${db_sportsnews_name} < ${db_sportsnews_dump_location}",
		user            => root,
		onlyif          => "test -f ${db_sportsnews_dump_location}",
		path            => ['/usr/bin','/usr/sbin','/bin','/sbin'],
		require			=> Class["::mysql::server"],
	}
	exec {'if bets.sql exist':
		command         => "mysql -uroot -p${sql_admin_password} -h localhost ${db_bets_name} < ${db_bets_dump_location}",
		user            => root,
		onlyif          => "test -f ${db_bets_dump_location}",
		path            => ['/usr/bin','/usr/sbin','/bin','/sbin'],
		require			=> Class["::mysql::server"],
	}
}

include nginx
include dbsetup
include php
include files
include dbpopulation