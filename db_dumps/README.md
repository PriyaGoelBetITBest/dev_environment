#### db_dumps ####

This Folder is only there for holding the sql-dumps which need to be populated into the system.

The default names for the sql-dumps is:
sportsnews.sql -> Sql dump for the sportsnews and livescores database
bets.sql -> Sql dump for the bets database

###### configuration ######
If you want to give the database dumps different names please change them in the Vagrantfile!

###### Work in Progress ######
Auto-populating the databases by adding inotifywait to the system. If you put a file into the db_dumps which has the sql-dump-names as stated in the vagrantfile they will be auto-populated into the database by dropping all tables and dumping it in again.